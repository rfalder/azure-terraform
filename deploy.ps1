[CmdletBinding()]
Param(
    [string] $DefaultSub = "",
    [string] $AzureLocation = "northeurope",
    [string] $AzureSubscriptions = "",
    [string] $GitlabServerURL = "https://gitlab.com/",
    [string] $GitlabRegistrationToken = ""
)

function Set-TFVar
{
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory = $true)]
        [string]$VariableName,
        [Parameter(Mandatory = $true)]
        [string]$Value
    )

    $fullName = "TF_VAR_$VariableName"
    [Environment]::SetEnvironmentVariable($fullName, $Value, "Process")
}

function Set-TFVars
{
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory = $true)]
        [hashtable]$Variables
    )

    foreach ($VariableName in $Variables.Keys) {
        Set-TFVar $VariableName $Variables[$VariableName]
    }
}


Set-TFVars @{
    "Location"  = $AzureLocation
    "Gitlab_Registration_Token" = $GitlabRegistrationToken
    "Gitlab_URL"       = $GitlabServerURL
}

try {
    az --version

if (!$?) {
    throw "Azure CLI not installed, please install it by following these instructions:  https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest."
}
}
catch {
    $_
}


try {
    terraform -version

if (!$?) {
    throw "Terraform not installed, please install it by following these instructions: https://www.terraform.io/downloads.html."
}
}
catch {
    $_
}

az login

az account set --subscription $DefaultSub

terraform init

terraform validate

terraform apply -auto-approve

$PrincipalID = terraform output container-principal-ID
echo $PrincipalID

# Maybe move this into terraform at some point
$AzureScriptionSplit = $AzureSubscriptions.Split(",")

foreach($AureSubscription in $AzureScriptionSplit) {
    az role assignment create --role "Contributor" --assignee-object-id $PrincipalID --subscription $AureSubscription

}
