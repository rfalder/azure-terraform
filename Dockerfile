FROM hashicorp/terraform:0.12.26

RUN apk add --no-cache \
    bash \
    ca-certificates \
    openssl \
    tzdata \
    curl \
    wget \
    jq \
    python3 \
    libc6-compat



COPY config.toml "/home/gitlab-runner/config.toml"
COPY startup.sh /usr/local/bin

ENV RUNNER="linuxAzureTerraform" \
    RUNNER_TAG_LIST="terraform, linux, AzureMSI" \
    RUNNER_BUILDS_DIR="/home/gitlab-runner" \
    REGISTRATION_TOKEN="" \
    CI_SERVER_URL="" \
    CONFIG_FILE="/home/gitlab-runner/config.toml" \
    REGISTER_NON_INTERACTIVE=true \
    RUNNER_EXECUTOR="shell" 

RUN curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64 && \
    adduser -D -S -h /home/gitlab-runner gitlab-runner && \
    chmod +x /usr/local/bin/gitlab-runner && \
    chmod +x /usr/local/bin/startup.sh

## Azure CLI install
RUN apk add --virtual=build gcc libffi-dev musl-dev openssl-dev python3-dev make cargo

RUN python3 -m pip install -U pip

RUN python3 -m pip install azure-cli

RUN apk del --purge build

## AZ Copy as well please 

ENV RELEASE_STAMP=20210226
ENV RELEASE_VERSION=10.9.0

RUN set -ex \
    && curl -L -o azcopy.tar.gz https://azcopyvnext.azureedge.net/release${RELEASE_STAMP}/azcopy_linux_amd64_${RELEASE_VERSION}.tar.gz \
    && tar -xzf azcopy.tar.gz && rm -f azcopy.tar.gz \
    && cp ./azcopy_linux_amd64_*/azcopy /usr/local/bin/. \
    && chmod +x /usr/local/bin/azcopy \
    && rm -rf azcopy_linux_amd64_*

WORKDIR /usr/local/bin    

ENTRYPOINT [ "startup.sh" ]