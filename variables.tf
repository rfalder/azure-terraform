
variable Rg_Name {
  type = string
  default     = "avt-int-terraform"
}

variable "Location" {
  type = string
  default     = "northeurope"
}

variable "Gitlab_Registration_Token" {
 type = string
  default     = ""
}

variable "Gitlab_URL" {
 type = string
  default     = "https://gitlab.com/"
}