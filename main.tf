provider "azurerm" {
  version = "~> 1.36.0"
}

resource "azurerm_resource_group" "avt-int-terraform" {
  name     = var.Rg_Name
  location = var.Location
}

resource "azurerm_container_group" "avt-int-terraform-container-instance" {
  name                = "${azurerm_resource_group.avt-int-terraform.name}-container-instance"
  location            = azurerm_resource_group.avt-int-terraform.location
  resource_group_name = azurerm_resource_group.avt-int-terraform.name
  os_type             = "Linux"
  restart_policy      = "OnFailure"
  ip_address_type     = "Public"

  identity {
    type = "SystemAssigned"
  }
  container {
    name   = "${azurerm_resource_group.avt-int-terraform.name}-container-instance-container"
    image  = "aventusrob/aventus-gitlab-terraform:latest"
    cpu    = "1"
    memory = "1.5"

    ports {
      port     = 443
      protocol = "TCP"
    }

    environment_variables = {
      REGISTRATION_TOKEN = var.Gitlab_Registration_Token
      CI_SERVER_URL      = var.Gitlab_URL
    }
  }

}
