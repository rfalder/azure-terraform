# Azure Terraform Runner

<a href="https://portal.azure.com/#create/Microsoft.Template/uri/https%3A%2F%2Faventus.gitlab.io%2Foperations%2Fazure-terraform-runner%2Fazuredeploy.json" target="_blank">
<img src="https://raw.githubusercontent.com/Azure/azure-quickstart-templates/master/1-CONTRIBUTION-GUIDE/images/deploytoazure.png"/>
</a>

https://aventus.gitlab.io/operations/azure-terraform-runner/azuredeploy.json

This template will deploy a Azure Terraform Runner for gitlab.

You will need your runner registration token and url for this instance to work.

Once deployed you will need to enable the System Identiy for this resource then assign it contributor rights to any subscription is needs to access.

